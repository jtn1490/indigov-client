import React from "react";
import request from "utils/request";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { get } from "lodash";
import validEmail from "utils/validEmail";

export default class SubmitQuestion extends React.Component {
  state = {
    email: ""
  };

  handleEmailChange(event) {
    const email = get(event, "target.value", "");
    this.setState({
      email
    });
  }

  async submitEmail(event) {
    event.preventDefault();

    const { email } = this.state;

    const requestOptions = {
      url: "https://mn0l5w2eti.execute-api.us-east-1.amazonaws.com/first",
      method: "post",
      data: {
        email
      }
    };
    request
      .makeRequest(requestOptions)
      .then(() => {
        this.props.onSubmitEmail({ success: true });
      })
      .catch(error => {
        this.props.onSubmitEmail({ success: false });
        console.error("error", error);
      });

    this.setState({
      email: ""
    });
  }

  render() {
    return (
      <div>
        <Form>
          <Form.Group as={Row}>
            <Form.Label column sm="2">
              Your Email
            </Form.Label>
            <Col sm="8">
              <Form.Control
                value={this.state.email}
                onChange={this.handleEmailChange.bind(this)}
                type="text"
                placeholder="foo@bar.com"
              />
            </Col>
            <Col sm="2">
              <Button
                onClick={this.submitEmail.bind(this)}
                disabled={!validEmail(this.state.email)}
                variant="primary"
                type="submit"
              >
                Submit
              </Button>
            </Col>
          </Form.Group>
        </Form>
      </div>
    );
  }
}
