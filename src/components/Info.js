import React from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
export default class Info extends React.Component {
  render() {
    return (
      <div>
        <Jumbotron>
          <h1>Hey there!</h1>
          <p>
            So like I mentioned during the interview, it's been quite a while
            since I've used React, and I've only used AWS lambdas and SQS
            services in proof of concept projects, and I don't have any public
            repositories to share. Therefore, I thought it'd be fun to build a
            very small sample app that utilizes React, AWS Lambdas, and AWS SQS.
            The purpose of this is three-fold (in no particular order):
          </p>
          <ul>
            <li>
              (1) To have something to stimulate my mind during the quarantine
            </li>
            <li>
              (2) Have something interesting to talk about during the technical
              interview
            </li>
            <li>(3) Repeat of reason #1</li>
          </ul>
          <p>
            So what does this app do? It's a simple app that lets users enter
            their email and get a joke sent to said email. The client sends the
            email request to a lambda that (1) adds a message to the SQS queue
            and (2) sends the user an email receipt. The SQS queue then triggers
            another lambda, which will retrieve a joke and email it to the user.
          </p>
          <p>Give it a try!</p>
        </Jumbotron>
      </div>
    );
  }
}
