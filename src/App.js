import React from "react";
import SubmitEmail from "components/SubmitEmail";
import Info from "components/Info";
import Alert from "react-bootstrap/Alert";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submittedAlert: false,
      alertType: null
    };
  }

  onSubmitEmail(results) {
    this.setState({
      submittedAlert: true,
      alertType: results.success ? "success" : "error"
    });

    // turn off alert
    setTimeout(() => {
      this.setState({
        submittedAlert: false
      });
    }, 4000);
  }

  render() {
    const { submittedAlert, alertType } = this.state;
    return (
      <div className="container">
        <Info />
        {submittedAlert ? (
          <Alert
            variant={alertType}
            onClose={() => this.setState({ submittedAlert: false })}
            dismissible
          >
            Check your email for a joke!
          </Alert>
        ) : null}
        <SubmitEmail
          onSubmitEmail={this.onSubmitEmail.bind(this)}
          dismissible
        />
      </div>
    );
  }
}
