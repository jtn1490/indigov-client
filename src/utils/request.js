import axios from "axios";
export default class Request {
  /**
   * Makes calls to services
   * @param {string} method HTTP Method
   * @param {string} route API route
   * @param {object} params HTTP parameters
   * @param {object} data HTTP request body
   * @param {object} customHeaders Additional HTTP headers
   */
  static async makeRequest(opts) {
    const { method, url, data, params, customHeaders } = opts;
    const requestHeaders = {
      ...customHeaders
    };
    return axios({
      url,
      method,
      headers: requestHeaders,
      data,
      params
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        console.log("request error", error);
        console.log("request error response", error.response);
        throw error.response;
      });
  }
}
