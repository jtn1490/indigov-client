module.exports = (email = "") => {
  const regex = /\S+@\S+\.\S+/;
  return regex.test(email);
};
